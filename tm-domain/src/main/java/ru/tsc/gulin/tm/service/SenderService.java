package ru.tsc.gulin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.service.ISenderService;
import ru.tsc.gulin.tm.dto.logger.EntityLogDTO;

import javax.jms.*;
import java.util.Date;

public class SenderService implements ISenderService {

    @NotNull
    private static final String TOPIC_NAME = "TM_LOG";

    @NotNull
    private final ConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull final EntityLogDTO log) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(TOPIC_NAME);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final ObjectMessage message = session.createObjectMessage(log);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityLogDTO createMessage(@NotNull final Object object, @NotNull final String type) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        @NotNull final String className = object.getClass().getSimpleName();
        @NotNull final EntityLogDTO message = new EntityLogDTO(className, new Date().toString(), json, type);
        return message;
    }

}
