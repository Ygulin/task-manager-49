package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonFasterXmlSaveRequest extends AbstractUserRequest {

    public DataJsonFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
