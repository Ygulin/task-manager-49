package ru.tsc.gulin.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.component.MessageExecutor;
import ru.tsc.gulin.tm.enumerated.EntityOperationType;

import javax.persistence.*;

@NoArgsConstructor
public class EntityListener {

    @NotNull
    private static final MessageExecutor MESSAGE_EXECUTOR = new MessageExecutor();

    @PostLoad
    public void postLoad(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_LOAD);
    }

    @PrePersist
    public void prePersist(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.PRE_PERSIST);
    }

    @PostPersist
    public void postPersist(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.PRE_REMOVE);
    }

    @PostRemove
    public void postRemove(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.PRE_UPDATE);
    }

    @PostUpdate
    public void postUpdate(@NotNull final Object entity) {
        sendMessage(entity, EntityOperationType.POST_UPDATE);
    }

    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
//        Bootstrap.sendMessage(entity, operationType.toString());
        MESSAGE_EXECUTOR.sendMessage(entity, operationType.toString());
    }

}
