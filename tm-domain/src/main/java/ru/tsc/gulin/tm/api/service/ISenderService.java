package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.logger.EntityLogDTO;

public interface ISenderService {

    void send(@NotNull EntityLogDTO log);

    @NotNull
    EntityLogDTO createMessage(@NotNull Object object, @NotNull String type);

}
