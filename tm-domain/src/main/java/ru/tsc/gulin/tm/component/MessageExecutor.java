package ru.tsc.gulin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.service.ISenderService;
import ru.tsc.gulin.tm.dto.logger.EntityLogDTO;
import ru.tsc.gulin.tm.service.SenderService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ISenderService service = new SenderService();

    @NotNull
    private final ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        executorService.submit(() -> {
            @NotNull final EntityLogDTO log = service.createMessage(object, type);
            service.send(log);
        });
    }

    public void stop() {
        executorService.shutdown();
    }

}
