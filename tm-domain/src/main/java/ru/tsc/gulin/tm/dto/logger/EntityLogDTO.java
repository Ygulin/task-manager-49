package ru.tsc.gulin.tm.dto.logger;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@XmlRootElement
public class EntityLogDTO implements Serializable {

    @NotNull
    private final String className;

    @NotNull
    private final String date;

    @NotNull
    private final String entity;

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    private final String type;

    public EntityLogDTO(
            @NotNull final String className,
            @NotNull final String date,
            @NotNull final String entity,
            @NotNull final String type
    ) {
        this.className = className;
        this.date = date;
        this.entity = entity;
        this.type = type;
    }

}
