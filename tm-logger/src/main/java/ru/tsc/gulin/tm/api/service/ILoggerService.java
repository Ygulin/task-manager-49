package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.logger.EntityLogDTO;

public interface ILoggerService {

    void writeLog(@NotNull EntityLogDTO message);

}
