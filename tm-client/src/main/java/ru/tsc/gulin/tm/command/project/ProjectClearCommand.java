package ru.tsc.gulin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear all projects";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        getProjectEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

}
