package ru.tsc.gulin.tm.endpoint;

import org.junit.experimental.categories.Category;
import ru.tsc.gulin.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class ProjectTaskEndpointTest {

    /* @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @Nullable
    private String token;

    @Nullable
    private Project initProject;

    @Nullable
    private Task initTask;

    @Before
    public void init() {
        token = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        initProject =
                projectEndpoint.createProject(new ProjectCreateRequest(token, "init project", "init")).getProject();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        initTask =
                taskEndpoint.createTask(new TaskCreateRequest(token, "init task", "init")).getTask();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest("", initProject.getId(), initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, "", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, initProject.getId(), "")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, "123", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(token, initProject.getId(), "123")
                )
        );
        @NotNull final TaskBindToProjectResponse response =
                projectTaskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, initProject.getId(), initTask.getId()));
        @NotNull final Task task = taskEndpoint.showTaskById(new TaskShowByIdRequest(token, initTask.getId())).getTask();
        Assert.assertEquals(initProject.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest("", initProject.getId(), initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, "", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, initProject.getId(), "")
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, "123", initTask.getId())
                )
        );
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindTaskFromProject(
                        new TaskUnbindFromProjectRequest(token, initProject.getId(), "123")
                )
        );
        @NotNull final TaskUnbindFromProjectResponse response =
                projectTaskEndpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(token, initProject.getId(), initTask.getId()));
        @NotNull final Task task = taskEndpoint.showTaskById(new TaskShowByIdRequest(token, initTask.getId())).getTask();
        Assert.assertNull(task.getProjectId());
    } */

}
