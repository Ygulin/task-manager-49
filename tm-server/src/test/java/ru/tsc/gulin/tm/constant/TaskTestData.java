package ru.tsc.gulin.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static ru.tsc.gulin.tm.constant.ProjectTestData.USER_PROJECT1;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static TaskDTO USER_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER_TASK3 = new TaskDTO();

    @NotNull
    public final static String NON_EXISTING_TASK_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<TaskDTO> USER_TASK_LIST = Arrays.asList(USER_TASK1, USER_TASK2, USER_TASK3);

    @NotNull
    public final static List<TaskDTO> TASK_LIST = new ArrayList<>();

    static {
        USER_TASK_LIST.forEach(task -> task.setName("User Test Task " + task.getId()));
        USER_TASK_LIST.forEach(task -> task.setDescription("User Test Task " + task.getId() + " description"));
        USER_TASK_LIST.forEach(task -> task.setProjectId(USER_PROJECT1.getId()));
        TASK_LIST.add(USER_TASK1);
        TASK_LIST.add(USER_TASK2);
    }

}
