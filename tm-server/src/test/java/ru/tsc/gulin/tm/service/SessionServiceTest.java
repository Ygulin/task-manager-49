package ru.tsc.gulin.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.api.service.IPropertyService;
import ru.tsc.gulin.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ISessionServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.gulin.tm.dto.model.SessionDTO;
import ru.tsc.gulin.tm.dto.model.UserDTO;
import ru.tsc.gulin.tm.exception.field.IdEmptyException;
import ru.tsc.gulin.tm.exception.field.UserIdEmptyException;
import ru.tsc.gulin.tm.marker.UnitCategory;
import ru.tsc.gulin.tm.migration.AbstractSchemeTest;
import ru.tsc.gulin.tm.service.dto.ProjectServiceDTO;
import ru.tsc.gulin.tm.service.dto.SessionServiceDTO;
import ru.tsc.gulin.tm.service.dto.TaskServiceDTO;
import ru.tsc.gulin.tm.service.dto.UserServiceDTO;

import java.util.List;

import static ru.tsc.gulin.tm.constant.SessionTestData.*;
import static ru.tsc.gulin.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.gulin.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @NotNull
    private static final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @NotNull
    private static final IUserServiceDTO userService = new UserServiceDTO(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static String userId = "";
    @NotNull
    private final ISessionServiceDTO service = new SessionServiceDTO(connectionService);

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);

        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        connectionService.close();
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, USER_SESSION3);
        });
        Assert.assertNotNull(service.add(userId, USER_SESSION3));
        @Nullable final SessionDTO session = service.findOneById(userId, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.getId());
    }

    @After
    public void clean() throws Exception {
        service.clear(userId);
    }

    @Before
    public void initTest() throws Exception {
        service.add(userId, USER_SESSION1);
        service.add(userId, USER_SESSION2);
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
        service.clear(userId);
        Assert.assertEquals(0, service.getSize(userId));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", NON_EXISTING_SESSION_ID));
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(userId, USER_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertFalse(service.existsById(userId, ""));
        Assert.assertFalse(service.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(service.existsById(userId, USER_SESSION1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
        final List<SessionDTO> sessions = service.findAll(userId);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(userId, session.getUserId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(userId, "");
        });
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = service.findOneById(userId, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", USER_SESSION1.getId());
        });
        Assert.assertNull(service.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = service.findOneById(userId, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
        Assert.assertEquals(2, service.getSize(userId));
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById(userId, "");
        });
        service.removeById(userId, USER_SESSION2.getId());
        Assert.assertNull(service.findOneById(userId, USER_SESSION2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        service.remove(userId, USER_SESSION2);
        Assert.assertNull(service.findOneById(userId, USER_SESSION2.getId()));
    }

}
