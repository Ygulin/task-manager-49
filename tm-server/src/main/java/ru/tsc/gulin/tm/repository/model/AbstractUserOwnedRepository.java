package ru.tsc.gulin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.gulin.tm.comparator.CreatedComparator;
import ru.tsc.gulin.tm.comparator.DateStartComparator;
import ru.tsc.gulin.tm.comparator.StatusComparator;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;
import ru.tsc.gulin.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<M> models = findAll(userId);
        if (models == null) return;
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        @NotNull final Root<M> root = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

}
