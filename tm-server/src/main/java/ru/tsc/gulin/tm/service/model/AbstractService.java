package ru.tsc.gulin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.model.IRepository;
import ru.tsc.gulin.tm.api.service.IConnectionService;
import ru.tsc.gulin.tm.exception.field.IdEmptyException;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IRepository<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            return repository.findAll(sort.getComparator());
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final M model) {
        if (model == null) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public void removeById(@Nullable final String id) {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    public void update(@Nullable final M model) {
        if (model == null) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
