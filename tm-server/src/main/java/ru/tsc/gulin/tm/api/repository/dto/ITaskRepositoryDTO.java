package ru.tsc.gulin.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    TaskDTO create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
