package ru.tsc.gulin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.enumerated.Status;
import ru.tsc.gulin.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    void changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    );

}
