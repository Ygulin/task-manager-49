package ru.tsc.gulin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.endpoint.*;
import ru.tsc.gulin.tm.api.service.*;
import ru.tsc.gulin.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ISessionServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.gulin.tm.api.service.model.IProjectTaskService;
import ru.tsc.gulin.tm.endpoint.*;
import ru.tsc.gulin.tm.service.*;
import ru.tsc.gulin.tm.service.dto.ProjectServiceDTO;
import ru.tsc.gulin.tm.service.dto.ProjectTaskServiceDTO;
import ru.tsc.gulin.tm.service.dto.TaskServiceDTO;
import ru.tsc.gulin.tm.service.dto.UserServiceDTO;
import ru.tsc.gulin.tm.service.dto.SessionServiceDTO;
import ru.tsc.gulin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskServiceDTO taskService = new TaskServiceDTO(connectionService);

    @Getter
    @NotNull
    private final ISessionServiceDTO sessionService = new SessionServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IProjectServiceDTO projectService = new ProjectServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskServiceDTO projectTaskService = new ProjectTaskServiceDTO(projectService, taskService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserServiceDTO userService = new UserServiceDTO(propertyService, connectionService, projectService, taskService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private  final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    public void run() {
        initPID();
        initJMS();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        backup.start();
    }

    private void prepareShutDown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN**");
        backup.stop();
    }

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initJMS() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    public void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name+ "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

}
