package ru.tsc.gulin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.dto.model.ProjectDTO;
import ru.tsc.gulin.tm.dto.model.TaskDTO;
import ru.tsc.gulin.tm.enumerated.Status;

import java.util.Date;
import java.util.List;

public interface ITaskServiceDTO extends IUserOwnedServiceDTO<TaskDTO> {

    void changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    );

}
